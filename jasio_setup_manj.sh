#!/bin/bash
# Generic contains function declaration
function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}

# Main body of code starts here
arg=$1

function showHelp {
    echo "Options:"
    echo " Choosing any of the valid options below will automatically install prerequesite software: curl, wget, and ctags"
    echo "  -s, --config            To setup config files (bash, vim)"
    echo "  -h, --help              Show this help section"
    echo "  -b, --bash              To setup Bash config files (BashRC and Bash_Logout) WARNING: This will overwrite your current Bash config"
    echo "  -v, --vim               To setup Vim config file (VimRC) WARNING: This will overwrite your current VimRC"
    echo ""
    echo " Choosing any of the options below will automatically install prerequesite software: snap package manager"
    echo "  -c, --code              To install coding languages, IDE's, and helpful coding Software like git"
    echo "  -a, --all               To execute all of the above at once"
}

[ -z $arg ] && showHelp

echo "THIS SCRIPT IS STILL UNDER DEVELOPMENT!";
echo "Exiting without making any changes...";
exit 1;

# Update references and upgrade system
sudo pacman -Syu --noconfirm;

# Audio control
sudo pacman -S pulseaudio pavucontrol --noconfirm;  # pulseaudio with GUI

# Video player and renderer
sudo pacman -S vlc kdenlive --noconfirm;

# PDF reader
sudo pacman -S evince --noconfirm;

# File managers
sudo pacman -S thunar ranger --noconfirm;  # GUI and Terminal file managers
ranger --copy-config=rifle && cp ./config_files/not_in_setup_file/file_managers/rifle.conf ~/.config/ranger && cp ./config_files/not_in_setup_file/file_managers/rc.conf ~/.config_files/ranger || echo "Could not configure ranger\nDid ranger install correctly?"

# Generic productivity
sudo pacman -S firefox libreoffice-still --noconfirm;  # Browser and libre-office stable as ms-office replacement

# Development
sudo pacman -S git neovim docker nodejs npm --noconfirm;  # Git for version control, docker for running and building docker images, and neovim text editor
# Neovim config file
mkdir -p ~/.config/nvim;
cp ./config_files/not_in_setup_file/nvim/init.vim ~/.config/nvim/

# Java dev
sudo pacman -S jdk11-openjdk intellij-idea-community-edition --noconfirm;  # Openjdk and my IDE of choice

# AUR packages
sudo pacman -S yay;
yay -Syu;

yay -S stacer;  # GUI system cleanup
yay -S kazam;  # Kazam for screen capture
yay -S ttf-ms-fonts;  # Microsoft fonts (Arial, Times New Roman...)
yay -S eclipse-java-bin  # Eclipse as extensible java IDE
