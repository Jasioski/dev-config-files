# I3 Config files

Here are my i3 config files.
* config (my i3 config with relevant key bindings)
* i3blocks.conf (my i3 status bar replacement)

Some features like brightness with fn keys, and making tap to click enabled on trackpad have separate scripts to make them work found in the [generic_scripts](https://gitlab.com/Jasioski/dev-config-files/-/tree/dev/config_files/not_in_setup_file/generic_scripts) directory.
